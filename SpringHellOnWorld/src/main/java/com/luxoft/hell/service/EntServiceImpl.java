package com.luxoft.hell.service;

import com.luxoft.hell.dao.EntDao;
import com.luxoft.hell.entity.Ent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 14.02.14.
 */
@Service
public class EntServiceImpl implements EntService{

    @Autowired
    EntDao entDao;

    @Override
    public void addEnt(Ent ent) {
        entDao.save(ent);
    }

    @Override
    public Ent getEntById(int id) {
        return entDao.findOne((long)id);
    }

    @Override
    public List<Ent> getAllEnts() {
        List<Ent> result = new ArrayList<Ent>();
        for(Ent e: entDao.findAll()){
            result.add(e);
        }
        return result;
    }
}
