package com.luxoft.hell.service;

import com.luxoft.hell.entity.Ent;

import java.util.List;

/**
 * Created by User on 14.02.14.
 */
public interface EntService {

    public void addEnt(Ent ent);
    public Ent getEntById(int id);
    public List<Ent> getAllEnts();

}
