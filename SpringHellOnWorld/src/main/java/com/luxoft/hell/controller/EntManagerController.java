package com.luxoft.hell.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by User on 14.02.14.
 */
@Controller
public class EntManagerController {

    @RequestMapping(value = "/com", method = RequestMethod.GET)
    public String example1(Model model)
    {
        return "classpath:common";
    }

}
