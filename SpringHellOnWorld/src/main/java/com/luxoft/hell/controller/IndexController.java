package com.luxoft.hell.controller;

import com.luxoft.hell.service.EntService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by User on 14.02.14.
 */
@Controller
public class IndexController {
    @Autowired
    EntService eS;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        model.addAttribute("message", "Hell on world");
        model.addAttribute("ents", eS.getAllEnts());
        return "thindex";
    }
}
