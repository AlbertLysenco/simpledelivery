package com.luxoft.hell.entity;

import javax.persistence.*;

/**
 * Created by User on 14.02.14.
 */
@Entity
@Table(name = "hell_ent")
public class Ent {
    @Id
    @GeneratedValue
    @Column(name = "ent_id")
    private int id;

    private String name;
    private String message;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
