package com.luxoft.hell.dao;

import com.luxoft.hell.entity.Ent;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by User on 14.02.14.
 */
//public interface TestRepository extends CrudRepository<Test, Long> {}
public interface EntDao extends CrudRepository<Ent, Long> {
}
