package com.luxoft.sdeli;

import com.luxoft.sdeli.config.MailConfig;
import com.luxoft.sdeli.config.PersistenceConfig;
import com.luxoft.sdeli.config.SecurityConfig;
import com.luxoft.sdeli.config.WebMvcConfig;
import com.luxoft.sdeli.dao.UserDao;
import com.luxoft.sdeli.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {PersistenceConfig.class, WebMvcConfig.class, SecurityConfig.class, MailConfig.class})
public class AppTests {
    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected WebApplicationContext wac;
	@Autowired
	UserDao userDao;
	private MockMvc mockMvc;
	@Autowired
	private UserService userService;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @Test
    public void simple() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void simple2() throws Exception { //by vx
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("message", "Hello"));
    }

    @Test
    public void testGetUserByLogin() throws Exception {
        assert userDao.findOne(1L).getLogin().equals(userService.getUserByLogin("admin").getLogin());
    }
}
