package com.luxoft.sdeli.service.helper;

import com.luxoft.sdeli.dto.RegistrationData;
import com.luxoft.sdeli.entity.User;
import com.luxoft.sdeli.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * User: vx
 * Date: 2/9/14
 * Time: 10:34 PM
 */
@Service
public class RegistrationValidator implements Validator {

	@Autowired
	private UserService userService;

	@Override
	public boolean supports(Class<?> aClass) {
		return RegistrationData.class.isAssignableFrom(aClass);

	}

	@Override
	public void validate(Object target, Errors errors) {
		if (((RegistrationData) target).getLogin() != null) {
			RegistrationData data = (RegistrationData) target;
			String login = data.getLogin();
			User user = userService.getUserByLogin(login);
			if (user != null) {
				errors.rejectValue("login", "loginAlreadyExist.login", "Username already in use.");
			}
			user = null;
			user = userService.getUserByEmail(data.getEmail());
			if (user != null) {
				errors.rejectValue("email", "emailAlreadyExist.email", "Email already in use.");
			}
		}
	}
}
