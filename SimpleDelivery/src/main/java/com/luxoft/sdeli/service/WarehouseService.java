package com.luxoft.sdeli.service;

import com.google.common.collect.Lists;
import com.luxoft.sdeli.dao.WarehouseDao;
import com.luxoft.sdeli.entity.Warehouse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: Albert Lysenco
 * Date: 29.03.14
 * Time: 22:38
 */
@Service
@Transactional
public class WarehouseService {

    @Autowired
    private WarehouseDao warehouseDao;

	public List<Warehouse> findAll() {
		return Lists.newArrayList(warehouseDao.findAll());
    }
	public Warehouse findById(long id) {
		return warehouseDao.findOne(id);
	}

	public List<Warehouse> getAllWarhouses() {
		return Lists.newArrayList(warehouseDao.findAll());
	}

    public void save(Warehouse warehouse){
        warehouseDao.save(warehouse);
    }

	public void save(List<Warehouse> warehouses) {
		warehouseDao.save(warehouses);
	}
}
