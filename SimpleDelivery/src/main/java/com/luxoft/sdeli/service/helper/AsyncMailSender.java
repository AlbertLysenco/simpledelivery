package com.luxoft.sdeli.service.helper;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

/**
 * User: vx(home)
 * Date: 3/26/2014
 * Time: 11:04 PM
 */
public class AsyncMailSender implements MailSender {
	private static Logger log = Logger.getLogger(AsyncMailSender.class);

	@Autowired
	private MailSender mailSender;

	@Autowired
	private TaskExecutor taskExecutor;

	public AsyncMailSender() {
	}

	public void send(SimpleMailMessage simpleMessage) throws MailException {
		taskExecutor.execute(new AsyncMailTask(simpleMessage));
	}

	public void send(SimpleMailMessage[] simpleMessages) throws MailException {
		for (SimpleMailMessage message : simpleMessages) {
			send(message);
		}
	}

	private class AsyncMailTask implements Runnable {

		private SimpleMailMessage message;

		private AsyncMailTask(SimpleMailMessage message) {
			this.message = message;
		}

		public void run() {
			mailSender.send(message);
		}
	}

}