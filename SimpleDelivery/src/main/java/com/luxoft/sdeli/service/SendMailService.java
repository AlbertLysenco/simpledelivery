package com.luxoft.sdeli.service;

import com.luxoft.sdeli.config.RootConfiguration;
import com.luxoft.sdeli.entity.Order;
import com.luxoft.sdeli.entity.User;
import com.luxoft.sdeli.service.helper.AsyncMailSender;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 * User: vx(home)
 * Date: 3/26/2014
 * Time: 11:07 PM
 */
@Service
@Import(RootConfiguration.class)
public class SendMailService {
	private static Logger log = Logger.getLogger(SendMailService.class);

	private
	@Value("${mail.sdeli.host}")
	String sdeliHost;
	private
	@Value("${mail.username}")
	String senderEmail;

	@Autowired
	private AsyncMailSender mailSender;


	public void sendEmail(String to, String subject, String body)
			throws MailException {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(to);
		message.setFrom(senderEmail);
		message.setSubject(subject);
		message.setText(body);
		mailSender.send(message);
	}

	public void sendConfirmEmail(User sendTo) {
		String subject = "Your account has been registered";
		String body = "To place orders you must validate your account\n" +
				"to confirm your email address go to link below:\n" +
				"http://" + sdeliHost + "/emailConfirm?user=" + sendTo.getValidationCode();
		sendEmail(sendTo.getEmail(), subject, body);
	}

	public void sendOrderSuccessEmail(User sendTo, Order order) {
		String subject = "Order success!";
		String body = "You order success and confirm\n" +
				"Your order number is #" + order.getNumber();
		sendEmail(sendTo.getEmail(), subject, body);
	}

	public void sendForgotEmail(User sendTo) {
		String subject = "Password recovery";
		String body = "To change your password go to following link\n" +
				"http://" + sdeliHost + "/changePasswordForm?user=" + sendTo.getValidationCode();
		sendEmail(sendTo.getEmail(), subject, body);
	}

	public void sendOrderIsWaiting(User sendTo, Order o) {
		String subject = "Your order " + o.getNumber() + " is delivered";
		String body = "Your order " + o.getNumber() + " is waiting for you to come pick it up\n" +
				"at our warehouse located in " + o.getDestination();
		sendEmail(sendTo.getEmail(), subject, body);
	}
}
