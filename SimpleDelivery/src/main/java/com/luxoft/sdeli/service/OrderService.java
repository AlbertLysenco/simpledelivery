package com.luxoft.sdeli.service;

import com.google.common.collect.Lists;
import com.luxoft.sdeli.dao.OrderDao;
import com.luxoft.sdeli.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by albertlysenco on 26.03.14.
 */
@Service
@Transactional
public class OrderService {

	@Autowired
	private OrderDao orderDao;

	public Order findById(long id) {
		Order order = orderDao.findOne(id);
		return order;
	}

	@Transactional
	public List<Order> findAllForUser(long userId) {
		List<Order> orders = orderDao.findByUserIdWithWarehouses(userId);
		return orders;
	}

	public void saveOrder(Order order) {
		orderDao.save(order);
	}

	public Order findByNumber(int number) {
		return orderDao.findByNumber(number);
	}

	public List<Order> findAll() {
		return Lists.newArrayList(orderDao.findAllWithWarehouses());
	}
}
