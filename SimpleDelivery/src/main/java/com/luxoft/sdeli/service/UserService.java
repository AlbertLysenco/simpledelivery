package com.luxoft.sdeli.service;

import com.google.common.collect.Lists;
import com.luxoft.sdeli.RoleConstants;
import com.luxoft.sdeli.dao.UserDao;
import com.luxoft.sdeli.dto.RegistrationData;
import com.luxoft.sdeli.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {


	@Autowired
	private UserDao userDao;

	@Autowired
	private SendMailService sendMailService;

	public void saveUser(User b) {
		userDao.save(b);
	}

	public void registerUser(RegistrationData d) {
		User u = new User();
		u.setLogin(d.getLogin());
		u.setPassword(d.getPassword());
		u.setEmail(d.getEmail());
		u.setName(d.getName());
		u.setRole(RoleConstants.ROLE_INACTIVE_USER);
		u.setValidationCode(u.hashCode());
		userDao.save(u);

		sendMailService.sendConfirmEmail(u);
	}

	public User getUserById(long id) {
		return userDao.findOne(id);
	}

	public User getUserByLogin(final String login) {
		return userDao.findByLogin(login);
	}

	public User getUserByEmail(final String email) {
		return userDao.findByEmail(email);
	}

	public List<User> getAllUsers() {
		return Lists.newArrayList(userDao.findAll());
	}

	public User getUserByValidationCode(int validationCode) {
		return userDao.findByValidationCode(validationCode);
	}

	public void deleteUserById(long id) {
		userDao.delete(id);
	}
}
