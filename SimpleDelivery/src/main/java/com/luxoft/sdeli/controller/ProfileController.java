package com.luxoft.sdeli.controller;

import com.luxoft.sdeli.dto.UserDto;
import com.luxoft.sdeli.entity.Order;
import com.luxoft.sdeli.entity.User;
import com.luxoft.sdeli.service.OrderService;
import com.luxoft.sdeli.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.List;

/**
 * User: Ilya Sernikov
 * Date: 26.03.2014
 * Time: 23:43
 */
@Controller
public class ProfileController {

	@Autowired
	private UserService userService;

	@Autowired
	private OrderService orderService;

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String printUserProfile(ModelMap modelMap, Principal principal) {
		if (principal == null) {
			return "login";
		}
		String userlogin = principal.getName();
		User currentUser = userService.getUserByLogin(userlogin);
		UserDto UserDto = new UserDto();
		UserDto.setName(currentUser.getName());
		UserDto.setLogin(currentUser.getLogin());
		UserDto.setEmail(currentUser.getEmail());
		UserDto.setPhone(currentUser.getPhone());

		List<Order> userOrders = orderService.findAllForUser(currentUser.getId());

		UserDto.setOrders(userOrders);

		modelMap.addAttribute("user", UserDto);

		return "profile";
	}
}
