package com.luxoft.sdeli.controller;

import com.luxoft.sdeli.OrderStatus;
import com.luxoft.sdeli.entity.Order;
import com.luxoft.sdeli.entity.Warehouse;
import com.luxoft.sdeli.service.OrderService;
import com.luxoft.sdeli.service.SendMailService;
import com.luxoft.sdeli.service.WarehouseService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by vx on 28.03.2014 .
 * Time: 14:27
 */
@Controller
public class ManagerController {
	private static Logger log = Logger.getLogger(ManagerController.class);

	@Autowired
	private OrderService orderService;

	@Autowired
	private WarehouseService warehouseService;

	@Autowired
	private SendMailService mailService;

	@RequestMapping(value = "/manage/orderList", method = RequestMethod.GET)
	public String orderList(ModelMap model) {
		model.put("orders", orderService.findAll());
		return "/management/orderList";
	}

	@RequestMapping(value = "/manage/changeOrderStatus", method = RequestMethod.POST)
	public String changeOrderStatusPost(ModelMap modelMap, @ModelAttribute("orderNumber") String orderNumber, @ModelAttribute("orderStatus") String orderStatus) {
		if (orderNumber.equals("")) {
			return "redirect:/manage/orderList";
		}
		int number = 0;
		try {
			number = Integer.valueOf(orderNumber);
		} catch (NumberFormatException ex) {
			return "redirect:/manage/orderList";
		}
		Order o = orderService.findByNumber(number);
		if (o != null) {
			o.setOrderStatus(OrderStatus.fromString(orderStatus));
			orderService.saveOrder(o);
			modelMap.put("success", true);
			modelMap.put("order", o);

			if (o.getOrderStatus() == OrderStatus.DELIVERED) {
				mailService.sendOrderIsWaiting(o.getUser(), o);
			}

		}
		return "redirect:/orderChangedSuccess?number=" + orderNumber;
	}

	@RequestMapping(value = "/manage/addNewLocation", method = RequestMethod.POST)
	public String addNewLocation(ModelMap modelMap, @ModelAttribute("orderNumber") String orderNumber, @ModelAttribute("warehouseName") String warehouseName) {
		if (warehouseName.equals("")) {
			System.out.println("NULL");
			return "redirect:/manage/orderList";
		}
		if (orderNumber.equals("")) {
			return "redirect:/manage/orderList";
		}
		int number = 0;
		try {
			number = Integer.valueOf(orderNumber);
		} catch (NumberFormatException ex) {
			return "redirect:/manage/orderList";
		}
		Order o = orderService.findByNumber(number);
		if (o != null) {
			int id = 0;
			try {
				id = Integer.parseInt(warehouseName);
			} catch (NumberFormatException ex) {
				return "redirect:/manage/orderList";
			}
			Warehouse wh = warehouseService.findById(id);
			o.getWarehouses().add(wh);
			orderService.saveOrder(o);

			modelMap.put("success", true);
			modelMap.put("order", o);
		}
		return "redirect:/orderChangedSuccess?number=" + orderNumber;
	}
}
