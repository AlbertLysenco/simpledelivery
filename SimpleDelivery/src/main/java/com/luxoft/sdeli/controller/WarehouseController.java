package com.luxoft.sdeli.controller;

import com.luxoft.sdeli.dto.WarehouseDto;
import com.luxoft.sdeli.entity.Warehouse;
import com.luxoft.sdeli.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * User: Albert Lysenco
 * Date: 29.03.14
 * Time: 23:27
 */
@Controller
public class WarehouseController {

    @Autowired
    private WarehouseService warehouseService;

    @ModelAttribute
    public WarehouseDto warehouseDto() {
        return new WarehouseDto();
    }


    @RequestMapping(value = "/manage/addWarehouse", method = RequestMethod.GET)
    public String addWarehouse() {
        return "management/addWarehouse";
    }

    @RequestMapping(value = "/manage/addWarehouse", method = RequestMethod.POST)
    public String addWarehousePost(@Valid WarehouseDto warehouseDto, BindingResult bindingResult, Principal principal, ModelMap modelMap) {

        if (bindingResult.hasErrors()) {
            System.out.println("error");
            return "management/addWarehouse";  // html  page
        }

        String  city = warehouseDto.getCity();
        String address = warehouseDto.getAddress();

        Warehouse warehouse = new Warehouse();
        warehouse.setCity(city);
        warehouse.setAddress(address);

        warehouseService.save(warehouse);

        return "redirect:/warehouses";
    }
}
