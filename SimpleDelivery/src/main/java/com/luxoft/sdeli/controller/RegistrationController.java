package com.luxoft.sdeli.controller;

import com.luxoft.sdeli.RoleConstants;
import com.luxoft.sdeli.dto.RegistrationData;
import com.luxoft.sdeli.entity.User;
import com.luxoft.sdeli.service.UserService;
import com.luxoft.sdeli.service.helper.CustomUserDetailService;
import com.luxoft.sdeli.service.helper.RegistrationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

/**
 * User: vx
 * Date: 2/9/14
 * Time: 10:53 PM
 */
@Controller
public class RegistrationController {
	@Autowired
	private UserService userService;
	@Autowired
	private RegistrationValidator registrationValidator;
	@Autowired
	private CustomUserDetailService userDetailService;
	@Autowired
	private BCryptPasswordEncoder encoder;

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String registration(Map model) {
		return "/registration/registration";
	}

	// @ModelAttribute("registrationData") tries to create registrationData from variables of POST form in html page
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public String processAddingUser(Map model, @ModelAttribute("registrationData") @Valid RegistrationData data, BindingResult errors) {
		ValidationUtils.invokeValidator(registrationValidator, data, errors);
		if (errors.hasErrors()) {
			model.put("data", data);
			model.put("errors", errors.getAllErrors());
			return "/registration/registration";
		}
		String psw = encoder.encode(data.getPassword());
		data.setPassword(psw);
		userService.registerUser(data);
		return "/registration/registrationSuccess";
	}

	@RequestMapping(value = "/registrationSuccess", method = RequestMethod.GET)
	public String registrationSuccess(Map model) {
		return "/registration/registrationSuccess";
	}

	@RequestMapping(value = "/emailConfirm", method = RequestMethod.GET)
	public String confirmEmail(Map model, @RequestParam("user") String userHash) {
		User u = userService.getUserByValidationCode(Integer.valueOf(userHash));
		if (u != null && u.getRole().equals(RoleConstants.ROLE_INACTIVE_USER)) {
			u.setRole(RoleConstants.ROLE_USER);
			userService.saveUser(u);
			Authentication authentication = new UsernamePasswordAuthenticationToken(
					userDetailService.loadUserByUsername(u.getLogin()),
					null,
					AuthorityUtils.createAuthorityList(u.getRole()));
			SecurityContextHolder.getContext().setAuthentication(authentication);
			return "/registration/emailConfirm";
		}
		return "redirect:/";
	}
}
