package com.luxoft.sdeli.controller;

import com.luxoft.sdeli.OrderStatus;
import com.luxoft.sdeli.dto.OrderDto;
import com.luxoft.sdeli.entity.Order;
import com.luxoft.sdeli.entity.User;
import com.luxoft.sdeli.entity.Warehouse;
import com.luxoft.sdeli.service.OrderService;
import com.luxoft.sdeli.service.SendMailService;
import com.luxoft.sdeli.service.UserService;
import com.luxoft.sdeli.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Albert Lysenco
 * Date: 26.03.14
 * Time: 23:37
 */

@Controller
public class OrderController {

	@Autowired
	private SendMailService sendMailService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private UserService userService;

	@Autowired
	private WarehouseService warehouseService;

	@ModelAttribute
	public OrderDto orderDto() {
		return new OrderDto();
	}

	@RequestMapping(value = "/createOrder", method = RequestMethod.GET)
	public String createOrder(ModelMap model) {
		model.addAttribute("warehouses", warehouseService.findAll());
		return "order/createOrder"; // name html page
	}

	@RequestMapping(value = "/orderSuccess", method = RequestMethod.GET)
	public String orderSuccess(ModelMap model) {
		return "order/orderSuccess";
	}

	@RequestMapping(value = "/createOrder", method = RequestMethod.POST)
	public String saveOrder(@Valid OrderDto orderDto, BindingResult bindingResult, Principal principal, ModelMap modelMap) {

		if (bindingResult.hasErrors()) {
			System.out.println("error");
			modelMap.addAttribute("warehouses", warehouseService.findAll());
			return "order/createOrder";
		}

		String startLocation = orderDto.getStartLocation();
		String destLocation = orderDto.getDestLocation();
		String weight = orderDto.getWeight();
		String width = orderDto.getWidth();
		String heigh = orderDto.getHeigh();
		String length = orderDto.getLength();
		String orderComment = orderDto.getOrderComment();

		String userLogin = principal.getName();
		User userByLogin = userService.getUserByLogin(userLogin);

		Order order = new Order();
		order.setUser(userByLogin);

		final Warehouse sending = warehouseService.findById(Long.parseLong(startLocation));
		Warehouse receiving = warehouseService.findById(Long.parseLong(destLocation));

		order.setWarehouses(new ArrayList<Warehouse>() {
			{
				add(sending);
			}
		});
		order.setDestination(receiving);

		order.setWeight(Double.parseDouble(weight));
		order.setWidth(Double.parseDouble(width));
		order.setHeigh(Double.parseDouble(heigh));
		order.setLength(Double.parseDouble(length));
		order.setOrderComment(orderComment);
		order.setOrderStatus(OrderStatus.INTRANSIT);
		order.setInitialReceiveMethod("rukami");
		order.setInitialReceiveInfo("info");
		order.setVolume(Double.parseDouble(length) * Double.parseDouble(heigh) * Double.parseDouble(width));
		order.setNumber(order.generateOrderNumber());
		System.out.println(order.hashCode());

		orderService.saveOrder(order);

		sendMailService.sendOrderSuccessEmail(userByLogin, order);
		return "redirect:/orderSuccess";
	}

}
