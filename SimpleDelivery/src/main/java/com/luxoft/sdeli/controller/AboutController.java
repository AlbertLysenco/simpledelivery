package com.luxoft.sdeli.controller;

import com.luxoft.sdeli.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: Roman Litvishko
 * Date: 26.03.14
 * Time: 23:56
 */

@Controller
public class AboutController {

	@Autowired
	private WarehouseService warehouseService;

	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String about(ModelMap model) {
		return "/simpleHtml/about";
	}

	@RequestMapping(value = "/warehouses", method = RequestMethod.GET)
	public String contacts(ModelMap model) {
		model.put("warehouses", warehouseService.findAll());

		return "warehouses";
	}
}
