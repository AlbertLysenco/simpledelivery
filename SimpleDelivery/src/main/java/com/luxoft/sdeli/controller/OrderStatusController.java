package com.luxoft.sdeli.controller;

import com.luxoft.sdeli.OrderStatus;
import com.luxoft.sdeli.entity.Order;
import com.luxoft.sdeli.service.OrderService;
import com.luxoft.sdeli.service.UserService;
import com.luxoft.sdeli.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: Roman Litvishko
 * Date: 27.03.14
 * Time: 00:04
 */

@Controller
public class OrderStatusController {
	@Autowired
	private OrderService orderService;

	@Autowired
	private UserService userService;

	@Autowired
	private WarehouseService warehouseService;

	@RequestMapping(value = "/orderStatus", method = RequestMethod.GET)
	public String orderStatus(ModelMap modelMap) {
		Order o = new Order();
		o.setId(0);
		modelMap.put("order", o);
		return "/order/orderStatus";
	}

	@RequestMapping(value = "/orderStatus", method = RequestMethod.POST)
	public String orderStatusPost(ModelMap modelMap, @ModelAttribute("orderNumber") String orderNumber) {
		if (orderNumber.equals("")) {
			Order o = new Order();
			o.setId(0);
			modelMap.put("order", o);
			return "/order/orderStatus";
		}
		int number = 0;
		try {
			number = Integer.valueOf(orderNumber);
		} catch (NumberFormatException ex) {
			return "/order/orderStatus";
		}
		Order o = orderService.findByNumber(number);
		if (o != null) {

			modelMap.put("order", o);
		}
		return "/order/orderStatus";
	}

	@RequestMapping(value = "/order", method = RequestMethod.GET)
	public String order(ModelMap modelMap, @ModelAttribute("number") String orderNumber) {
		if (orderNumber.equals("")) {
			Order o = new Order();
			o.setId(0);
			modelMap.put("order", o);
			return "/order/order";
		}
		int number = 0;
		try {
			number = Integer.valueOf(orderNumber);
		} catch (NumberFormatException ex) {
			return "/order/order";
		}
		Order o = orderService.findByNumber(number);
		if (o != null) {
			modelMap.addAttribute("warehouses", warehouseService.findAll());
			modelMap.put("order", o);
		}
		modelMap.put("OrderStatus", OrderStatus.values());
		return "/order/order";
	}

	@RequestMapping(value = "/orderChangedSuccess", method = RequestMethod.GET)
	public String orderChangedSuccess(ModelMap modelMap, @ModelAttribute("number") String orderNumber) {
		if (orderNumber.equals("")) {
			Order o = new Order();
			o.setId(0);
			modelMap.put("order", o);
			return "/order/orderChangedSuccess";
		}
		int number = 0;
		try {
			number = Integer.valueOf(orderNumber);
		} catch (NumberFormatException ex) {
			return "/order/orderChangedSuccess";
		}
		Order o = orderService.findByNumber(number);
		if (o != null) {
			modelMap.addAttribute("warehouses", warehouseService.findAll());
			modelMap.put("order", o);
		}
		modelMap.put("OrderStatus", OrderStatus.values());
		return "/order/orderChangedSuccess";
	}

}