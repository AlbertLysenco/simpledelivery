package com.luxoft.sdeli.controller;

import com.luxoft.sdeli.RoleConstants;
import com.luxoft.sdeli.dto.UserIdNewRoleDto;
import com.luxoft.sdeli.dto.UserSimpleDto;
import com.luxoft.sdeli.entity.User;
import com.luxoft.sdeli.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vx on 28.03.2014 .
 * Time: 14:27
 */
@Controller
public class AdminController {
	private static Logger log = Logger.getLogger(AdminController.class);

	@Autowired
	private UserService userService;

	@ModelAttribute
	public UserIdNewRoleDto userIdNewRoleDto() {
		return new UserIdNewRoleDto();
	}

	@RequestMapping(value = "/admin/managing", method = RequestMethod.GET)
	public String manageUsers(ModelMap model) {
		List<User> users = userService.getAllUsers();
		List<UserSimpleDto> usersDto = new ArrayList<>();
		List<String> roles = new ArrayList<String>() {
			{
				add(RoleConstants.ROLE_ADMIN);
				add(RoleConstants.ROLE_MANAGER);
				add(RoleConstants.ROLE_USER);
				add(RoleConstants.ROLE_INACTIVE_USER);
			}
		};

		for (User user : users) {
			UserSimpleDto userDto = new UserSimpleDto();
			userDto.setId(user.getId());
			userDto.setLogin(user.getLogin());
			userDto.setName(user.getName());
			userDto.setEmail(user.getEmail());
			userDto.setRole(user.getRole());

			usersDto.add(userDto);
		}

		model.addAttribute("users", usersDto);
		model.addAttribute("roles", roles);
		return "/management/managingUsers";
	}

	@RequestMapping(value = {"/admin/managing/delete"}, method = RequestMethod.GET)
	public String deleteUser(@RequestParam("id") String id) {
		userService.deleteUserById(Long.parseLong(id));
		return "redirect:/admin/managing";
	}

	@RequestMapping(value = "/admin/managing", method = RequestMethod.POST)
	public String modifyUserRole(@Valid UserIdNewRoleDto userIdNewRoleDto, BindingResult bindingResult, ModelMap modelMap) {

		if (bindingResult.hasErrors()) {
			log.error("Validation error!");
			return "redirect:/admin/managing";
		}

		User user = userService.getUserById(userIdNewRoleDto.getId());
		user.setRole(userIdNewRoleDto.getNewRole());

		userService.saveUser(user);
		return "redirect:/admin/managing";
	}
}
