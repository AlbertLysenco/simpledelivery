package com.luxoft.sdeli.controller;

import com.luxoft.sdeli.RoleConstants;
import com.luxoft.sdeli.entity.User;
import com.luxoft.sdeli.service.SendMailService;
import com.luxoft.sdeli.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by vx on 01.04.2014 .
 * Time: 12:14
 */
@Controller
public class ForgotEmailController {
	private static Logger log = Logger.getLogger(ForgotEmailController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private SendMailService sendMailService;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@RequestMapping(value = "/forgotEmail", method = RequestMethod.GET)
	public String forgotEmail(ModelMap modelMap) {
		return "/registration/forgotEmail";
	}

	@RequestMapping(value = "/forgotEmail", method = RequestMethod.POST)
	public String forgotEmailPost(ModelMap modelMap, @ModelAttribute("email") String email) {
		User u = userService.getUserByEmail(email);
		if (u != null) {
			u.setRole(RoleConstants.ROLE_INACTIVE_USER);
			userService.saveUser(u);
			sendMailService.sendForgotEmail(u);
		}
		return "/registration/forgotEmailSuccess";
	}

	@RequestMapping(value = "/changePasswordForm", method = RequestMethod.GET)
	public String changePasswordForm(ModelMap modelMap, @ModelAttribute("user") String userHash) {
		int hash = 0;
		try {
			hash = Integer.valueOf(userHash);
		} catch (NumberFormatException ex) {
			return "redirect:/";
		}
		User u = userService.getUserByValidationCode(hash);
		if (u.getRole().equals(RoleConstants.ROLE_INACTIVE_USER)) {
			modelMap.put("user", userHash);
			return "/registration/changePasswordForm";
		} else {
			return "redirect:/";
		}
	}

	@RequestMapping(value = "/changePasswordForm", method = RequestMethod.POST)
	public String changePasswordFormPost(ModelMap modelMap, @ModelAttribute("user") String userHash, @ModelAttribute("password") String password) {
		User u = userService.getUserByValidationCode(Integer.valueOf(userHash));
		if (u.getRole().equals(RoleConstants.ROLE_INACTIVE_USER)) {
			u.setPassword(encoder.encode(password));
			u.setRole(RoleConstants.ROLE_USER);
			userService.saveUser(u);
			return "/registration/changePasswordSuccess";
		} else {
			return "redirect:/";
		}
	}
}
