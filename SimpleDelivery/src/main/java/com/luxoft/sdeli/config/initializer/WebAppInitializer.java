package com.luxoft.sdeli.config.initializer;

import com.luxoft.sdeli.config.MailConfig;
import com.luxoft.sdeli.config.PersistenceConfig;
import com.luxoft.sdeli.config.SecurityConfig;
import com.luxoft.sdeli.config.WebMvcConfig;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;

/**
 * Created by vx on 13.02.14.
 */
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{PersistenceConfig.class, SecurityConfig.class, MailConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebMvcConfig.class};
    }

    @Override
    protected Filter[] getServletFilters() {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(false);
        return new Filter[]{characterEncodingFilter};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }


}
