package com.luxoft.sdeli.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.beans.PropertyVetoException;
import java.util.Properties;

/**
 * Created by vx on 13.02.14.
 */
@Configuration
@EnableJpaRepositories("com.luxoft.sdeli.dao")
// probably == <tx:annotation-driven transaction-manager="transactionManager" />
@EnableTransactionManagement
@Import(RootConfiguration.class)
public class PersistenceConfig {

    private
    @Value("${hibernate.connection.url}")
    String jdbcUrl;
    private
    @Value("${hibernate.connection.username}")
    String username;
    private
    @Value("${hibernate.connection.password}")
    String password;
    private
    @Value("${hibernate.connection.driver_class}")
    String driver;
    private
    @Value("${hibernate.c3p0.min_size}")
    int minPoolSize;
    private
    @Value("${hibernate.c3p0.max_size}")
    int maxPoolSize;
    private
    @Value("${hibernate.c3p0.timeout}")
    int timeout;
    private
    @Value("${hibernate.dialect}")
    String dialect;

    public PersistenceConfig() {
    }

    // Jpa + hibernate
    @Bean
    public ComboPooledDataSource dataSource() throws PropertyVetoException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setUser(username);
        dataSource.setPassword(password);
        dataSource.setDriverClass(driver);
        dataSource.setJdbcUrl(jdbcUrl);
        dataSource.setInitialPoolSize(1);
        dataSource.setMaxPoolSize(maxPoolSize);
        dataSource.setMinPoolSize(minPoolSize);
        dataSource.setAcquireIncrement(1);
        dataSource.setTestConnectionOnCheckin(false);
        dataSource.setTestConnectionOnCheckout(true);
        dataSource.setMaxIdleTime(timeout);
        dataSource.setIdleConnectionTestPeriod(1000);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws PropertyVetoException {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setDatabasePlatform(dialect);
        jpaVendorAdapter.setShowSql(true);
        jpaVendorAdapter.setGenerateDdl(true);
		jpaVendorAdapter.setShowSql(true);

        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        entityManagerFactoryBean.setPackagesToScan("com.luxoft.sdeli.entity");
        entityManagerFactoryBean.setJpaProperties(additionalProperties());

        return entityManagerFactoryBean;
    }

    @Bean
    public Properties additionalProperties() {
        Properties p = new Properties();
        p.setProperty("hibernate.dialect", dialect);
		p.setProperty("hibernate.hbm2ddl.auto", "validate");
		p.setProperty("javax.persistence.jdbc.url", jdbcUrl);
        return p;
    }

    @Bean
    public PlatformTransactionManager transactionManager() throws PropertyVetoException {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }
}
