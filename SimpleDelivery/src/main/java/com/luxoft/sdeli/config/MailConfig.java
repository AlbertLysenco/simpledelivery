package com.luxoft.sdeli.config;

import com.luxoft.sdeli.service.helper.AsyncMailSender;
import org.apache.commons.lang.CharEncoding;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.Properties;

/**
 * User: vx(home)
 * Date: 3/26/2014
 * Time: 10:41 PM
 */
@Configuration
@Import(RootConfiguration.class)
public class MailConfig {
	private static Logger log = Logger.getLogger(MailConfig.class);

	private
	@Value("${mail.host}")
	String host;
	private
	@Value("${mail.port}")
	int port;
	private
	@Value("${mail.username}")
	String username;
	private
	@Value("${mail.password}")
	String password;
	private
	@Value("${mail.smtp.auth}")
	String smtpAuth;
	private
	@Value("${mail.smtp.starttls.enable}")
	String startTlsEnable;


	@Bean
	public JavaMailSenderImpl mailSender() {
		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		sender.setHost(host);
		sender.setPort(port);
		sender.setUsername(username);
		sender.setPassword(password);
		sender.setDefaultEncoding(CharEncoding.UTF_8);
		sender.setJavaMailProperties(additionalMailProperties());
		return sender;
	}

	@Bean
	public Properties additionalMailProperties() {
		Properties p = new Properties();
		p.setProperty("mail.smtp.auth", smtpAuth);
		p.setProperty("mail.smtp.starttls.enable", startTlsEnable);
		return p;
	}

	@Bean
	public ThreadPoolTaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(5);
		executor.setMaxPoolSize(10);
		executor.setQueueCapacity(100);
		executor.setWaitForTasksToCompleteOnShutdown(true);
		return executor;
	}

	@Bean
	public AsyncMailSender asyncMailSender() {
		return new AsyncMailSender();
	}
}
