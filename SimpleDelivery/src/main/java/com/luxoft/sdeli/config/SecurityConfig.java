package com.luxoft.sdeli.config;

import com.luxoft.sdeli.RoleConstants;
import com.luxoft.sdeli.service.helper.CustomUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by vx on 19.02.14 .
 * Time: 15:36
 */
@Configuration
@EnableWebMvcSecurity
@ComponentScan(basePackages = {"com.luxoft.sdeli"},
		excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, value = Configuration.class)})
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Bean
	@Override
	public UserDetailsService userDetailsServiceBean() throws Exception {
		return new CustomUserDetailService();
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
				.authenticationProvider(authenticationProvider());
	}

	@Bean
	public AuthenticationProvider authenticationProvider() throws Exception {
		DaoAuthenticationProvider prov = new DaoAuthenticationProvider();
		prov.setUserDetailsService(userDetailsServiceBean());
		prov.setPasswordEncoder(bCryptPasswordEncoder());
		return prov;
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web
				.ignoring()
				.antMatchers("/resources/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.csrf().disable()
				.exceptionHandling().accessDeniedPage("/accessDenied").and()
				.formLogin().loginPage("/login").defaultSuccessUrl("/").permitAll().and()
				.authenticationProvider(authenticationProvider())
				.rememberMe().and()
				.logout().logoutUrl("/logout").logoutSuccessUrl("/").permitAll().and()
				.authorizeRequests()
				.antMatchers("/login", "/profile", "/about",
						"/warehouses", "/howToOrder", "/order", "/orderStatus",
						"/registration", "/registrationSuccess", "/emailConfirm",
						"/forgotEmail", "/changePasswordForm",
						"/changePasswordSuccess",
						"/", "/resources/**").permitAll()
				.antMatchers("/managing/**", "/admin/**").access("hasRole('" + RoleConstants.ROLE_ADMIN + "')")
				.antMatchers("/manage/**").access("hasRole('" + RoleConstants.ROLE_MANAGER + "')" +
				"or hasRole('" + RoleConstants.ROLE_ADMIN + "')")
//				.anyRequest().authenticated()
				.anyRequest().access("hasRole('" + RoleConstants.ROLE_USER + "')" +
				"or hasRole('" + RoleConstants.ROLE_ADMIN + "')" +
				"or hasRole('" + RoleConstants.ROLE_MANAGER +
				"')");
	}

}
