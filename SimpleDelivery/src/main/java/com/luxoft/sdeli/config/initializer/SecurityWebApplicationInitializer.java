package com.luxoft.sdeli.config.initializer;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by vx on 24.02.14 .
 * Time: 17:38
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
}
