package com.luxoft.sdeli.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * User: Ilya Sernikov
 * Date: 02.04.2014
 * Time: 16:22
 */
public class UserIdNewRoleDto {

	@NotNull
	private long id;
	@NotEmpty
	private String newRole;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNewRole() {
		return newRole;
	}

	public void setNewRole(String newRole) {
		this.newRole = newRole;
	}
}
