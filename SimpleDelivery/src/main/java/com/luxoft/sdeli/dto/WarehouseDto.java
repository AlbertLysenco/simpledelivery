package com.luxoft.sdeli.dto;

import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * User: Roman Litvishko
 * Date: 01.04.14
 * Time: 16:20
 */
public class WarehouseDto {

    @NotEmpty
    @Pattern(regexp = "[A-Z]{1}[a-z]+", message = "For example Kyiv") // Kyiv
    private String city;
    @NotEmpty
    @Pattern(regexp = "[A-Z]{1}[a-z]+[,][ ]{0,1}[0-9]+", message = "For example Kalinina, 6  or Kalinina,6")  // Kalinina, 6  or Kalinina,6
    private String address;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
