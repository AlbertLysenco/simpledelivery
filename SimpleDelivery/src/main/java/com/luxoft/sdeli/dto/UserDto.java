package com.luxoft.sdeli.dto;

import com.luxoft.sdeli.entity.Order;

import java.io.Serializable;
import java.util.List;

/**
 * User: Ilya Sernikov
 * Date: 27.03.2014
 * Time: 13:03
 */
public class UserDto implements Serializable {

	private String name;
	private String login;
	private String email;
	private String phone;

	private List<Order> orders;

	public UserDto() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
}
