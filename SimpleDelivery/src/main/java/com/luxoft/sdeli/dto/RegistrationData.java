package com.luxoft.sdeli.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

/**
 * User: vx
 * Date: 2/9/14
 * Time: 7:17 PM
 */
public class RegistrationData {

	@NotEmpty(message = "Cannot be empty")
	@Pattern(regexp = "[A-Za-z0-9]{1,30}", message = "login contains bad characters or too large")
	private String login;

	@NotEmpty(message = "Cannot be empty")
	private String password;

	@NotEmpty(message = "Cannot be empty")
	private String name;

	@Email(message = "Wrong email")
	@Pattern(regexp = "[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\\.[a-zA-Z]{2,4}", message = "Wrong email")
	private String email;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
}
