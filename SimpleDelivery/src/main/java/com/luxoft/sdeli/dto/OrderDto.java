package com.luxoft.sdeli.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

/**
 * User: Albert Lysenco
 * Date: 28.03.14
 * Time: 13:52
 */
public class OrderDto {

	@NotEmpty
	private String startLocation;
	@NotEmpty
	private String destLocation;
	@NotEmpty
	@Pattern(regexp = "[0-9]+([.]{0,1}[0-9]+){0,1}", message = "weight not valid")
	private String weight;
	@NotEmpty
	@Pattern(regexp = "[0-9]+([.]{0,1}[0-9]+){0,1}", message = "heigh not valid")
	private String heigh;
	@NotEmpty
	@Pattern(regexp = "[0-9]+([.]{0,1}[0-9]+){0,1}", message = "length not valid")
	private String length;
	@NotEmpty
	@Pattern(regexp = "[0-9]+([.]{0,1}[0-9]+){0,1}", message = "width not valid")
	private String width;

	private String orderComment;

	public String getStartLocation() {
		return startLocation;
	}

	public void setStartLocation(String startLocation) {
		this.startLocation = startLocation;
	}

	public String getDestLocation() {
		return destLocation;
	}

	public void setDestLocation(String destLocation) {
		this.destLocation = destLocation;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getHeigh() {
		return heigh;
	}

	public void setHeigh(String heigh) {
		this.heigh = heigh;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getOrderComment() {
		return orderComment;
	}

	public void setOrderComment(String orderComment) {
		this.orderComment = orderComment;
	}
}
