package com.luxoft.sdeli.dto;

import java.io.Serializable;

/**
 * User: Ilya Sernikov
 * Date: 02.04.2014
 * Time: 1:59
 */
public class UserSimpleDto implements Serializable {

	private long id;
	private String login;
	private String name;
	private String email;
	private String role;

	public UserSimpleDto() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
