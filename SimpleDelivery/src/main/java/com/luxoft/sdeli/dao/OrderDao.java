package com.luxoft.sdeli.dao;

import com.luxoft.sdeli.entity.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by albertlysenco on 26.03.14.
 */
public interface OrderDao extends CrudRepository<Order, Long> {
	public Order findById(long id);

	@Query("select o from Order o join fetch o.warehouses where o.user.id=?1 group by o")
	public List<Order> findByUserIdWithWarehouses(long userId);

	@Query("select o from Order o join fetch o.warehouses where o.number=?1")
	public Order findByNumber(int number);

	@Query("select o from Order o left join fetch o.warehouses GROUP BY o")
	public List<Order> findAllWithWarehouses();

}
