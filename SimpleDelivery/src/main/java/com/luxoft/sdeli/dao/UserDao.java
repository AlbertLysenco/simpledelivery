package com.luxoft.sdeli.dao;

import com.luxoft.sdeli.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * User: vx
 * Date: 2/8/14
 * Time: 4:16 PM
 */
//public interface UserDao extends CrudRepository<User, Long> {}
public interface UserDao extends CrudRepository<User, Long> {
	User findByLogin(String login);

	User findByEmail(String email);

	User findByValidationCode(int validationCode);
}
