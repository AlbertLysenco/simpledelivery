package com.luxoft.sdeli.dao;

import com.luxoft.sdeli.entity.Warehouse;
import org.springframework.data.repository.CrudRepository;

/**
 * User: Albert Lysenco
 * Date: 29.03.14
 * Time: 22:37
 */
public interface WarehouseDao extends CrudRepository<Warehouse, Long> {
//	Warehouse findById(long id);

//	@Query("select distinct o from Order o join fetch o.warehouses")
//	List<Order> findAllWithWH();
}
