package com.luxoft.sdeli.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * User: vx
 * Date: 2/8/14
 * Time: 4:15 PM
 */
@Entity
@Table(name = "users")
public class User implements Serializable {
	private static final long serialVersionUID = 7395183444219874363L;
	@Id
	@GeneratedValue
	@Column(name = "id")
	private long id;
	private String name;
	private String login;
	private String password;
	private String role;
	private String email;

	private String phone;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<Order> orders;

	private int validationCode;

	public User(String login, String password, String name, String role, String email, String phone) {
		super();
		this.name = name;
		this.role = role;
		this.setPassword(password);
		this.setLogin(login);
		this.email = email;
		this.phone = phone;
	}

	public User() {
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		User user = (User) o;

		if (email != null ? !email.equals(user.email) : user.email != null) return false;
		if (login != null ? !login.equals(user.login) : user.login != null) return false;
		if (name != null ? !name.equals(user.name) : user.name != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (login != null ? login.hashCode() : 0);
		result = 31 * result + (email != null ? email.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", name='" + name + '\'' +
				", login='" + login + '\'' +
				", password='" + password + '\'' +
				", role='" + role + '\'' +
				", email='" + email + '\'' +
				", phone='" + phone + '\'' +
				'}';
	}

	public int getValidationCode() {
		return validationCode;
	}

	public void setValidationCode(int validationCode) {
		this.validationCode = validationCode;
	}
}
