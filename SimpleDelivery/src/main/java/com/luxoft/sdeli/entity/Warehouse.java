package com.luxoft.sdeli.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * User: Albert Lysenco
 * Date: 29.03.14
 * Time: 22:35
 */
@Entity
@Table(name = "warehouse")
public class Warehouse implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	private String city;

	private String address;

	@ManyToMany(mappedBy = "warehouses", cascade = CascadeType.ALL)
	private List<Order> orders;

	public Warehouse() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Warehouse)) return false;

		Warehouse warehouse = (Warehouse) o;

		if (id != warehouse.id) return false;
		if (address != null ? !address.equals(warehouse.address) : warehouse.address != null) return false;
		if (city != null ? !city.equals(warehouse.city) : warehouse.city != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + (city != null ? city.hashCode() : 0);
		result = 31 * result + (address != null ? address.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "" + city + ", " + address;
	}
}
