package com.luxoft.sdeli.entity;

import com.luxoft.sdeli.OrderStatus;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * Created by albertlysenco on 26.03.14.
 */
@Entity
@Table(name = "orders")
public class Order {

	@Transient
	private UUID uuid;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	private int number;

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	private User user;

	@ManyToMany
	@JoinTable(name = "orders_warehouses", joinColumns = {@JoinColumn(name = "order_id")}, inverseJoinColumns = {@JoinColumn(name = "warehouse_id")})
	private List<Warehouse> warehouses;

	@OneToOne
	@JoinColumn(name = "warehouse_dest_id", referencedColumnName = "id", nullable = false)
	private Warehouse destination;

	private String initialReceiveMethod;

	private String initialReceiveInfo;

	private double weight;

	private double heigh;

	private double length;

	private double width;

	//    not saving in DB
	private double volume;

	private String orderComment;

	private String location;

	private OrderStatus orderStatus;

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getInitialReceiveMethod() {
		return initialReceiveMethod;
	}

	public void setInitialReceiveMethod(String initialReceiveMethod) {
		this.initialReceiveMethod = initialReceiveMethod;
	}

	public String getInitialReceiveInfo() {
		return initialReceiveInfo;
	}

	public void setInitialReceiveInfo(String initialReceiveInfo) {
		this.initialReceiveInfo = initialReceiveInfo;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeigh() {
		return heigh;
	}

	public void setHeigh(double heigh) {
		this.heigh = heigh;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public String getOrderComment() {
		return orderComment;
	}

	public void setOrderComment(String orderComment) {
		this.orderComment = orderComment;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Warehouse> getWarehouses() {
		return warehouses;
	}

	public void setWarehouses(List<Warehouse> warehouses) {
		this.warehouses = warehouses;
	}

	public Warehouse getDestination() {
		return destination;
	}

	public void setDestination(Warehouse destination) {
		this.destination = destination;
	}

	public int generateOrderNumber() {
		uuid = UUID.randomUUID();
		int uuidHash = uuid.hashCode();
		int orderHash = this.hashCode();
		int number = 31 * orderHash + uuidHash;
		return Math.abs(number);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Order)) return false;

		Order order = (Order) o;

		if (Double.compare(order.heigh, heigh) != 0) return false;
		if (id != order.id) return false;
		if (Double.compare(order.length, length) != 0) return false;
		if (number != order.number) return false;
		if (warehouses != order.warehouses) return false;
		if (destination != order.destination) return false;
		if (Double.compare(order.volume, volume) != 0) return false;
		if (Double.compare(order.weight, weight) != 0) return false;
		if (Double.compare(order.width, width) != 0) return false;
		if (initialReceiveInfo != null ? !initialReceiveInfo.equals(order.initialReceiveInfo) : order.initialReceiveInfo != null)
			return false;
		if (initialReceiveMethod != null ? !initialReceiveMethod.equals(order.initialReceiveMethod) : order.initialReceiveMethod != null)
			return false;
		if (location != null ? !location.equals(order.location) : order.location != null) return false;
		if (orderComment != null ? !orderComment.equals(order.orderComment) : order.orderComment != null) return false;
		if (orderStatus != order.orderStatus) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		result = (int) (id ^ (id >>> 32));
		result = 31 * result + user.hashCode();
		result = 31 * result + destination.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return "Order{" +
				"id=" + id +
				", number=" + number +
				", send from=" + warehouses.get(0) +
				", receive to=" + destination +
				", initialReceiveMethod='" + initialReceiveMethod + '\'' +
				", initialReceiveInfo='" + initialReceiveInfo + '\'' +
				", weight=" + weight +
				", heigh=" + heigh +
				", length=" + length +
				", width=" + width +
				", volume=" + volume +
				", orderComment='" + orderComment + '\'' +
				", location='" + location + '\'' +
				", orderStatus=" + orderStatus +
				'}';
	}
}
