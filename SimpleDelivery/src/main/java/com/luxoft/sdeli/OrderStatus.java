package com.luxoft.sdeli;

/**
 * Created by albertlysenco on 26.03.14.
 */
public enum OrderStatus {
	UNDEFINED, DELIVERED, INTRANSIT, RECEIVED, INWAREHOUSE, WAITING;

	public static OrderStatus fromString(String status) {
		switch (status) {
			case "DELIVERED":
				return OrderStatus.DELIVERED;
			case "INTRANSIT":
				return OrderStatus.INTRANSIT;
			case "RECEIVED":
				return OrderStatus.RECEIVED;
			case "INWAREHOUSE":
				return OrderStatus.INWAREHOUSE;
			case "WAITING":
				return OrderStatus.WAITING;
			default:
				return OrderStatus.UNDEFINED;
		}
	}
}
