function validate(){
var login = $("#login").val();
var password = $("#password").val();
var passwordConfirm = $("#passwordConfirm").val();
var name = $("#name").val();
var email = $("#email").val();
var result = true;

if (login.length < 1){
$("#loginGroup").removeClass("has-success")
$("#loginGroup").addClass("has-error")
$("#loginErrorEmpty").removeClass("hidden")
result = false;
} else
if (login.length > 29){
$("#loginGroup").removeClass("has-success")
$("#loginGroup").addClass("has-error")
$("#loginErrorLength").removeClass("hidden")
result = false;
} else
if (!login.match("[A-Za-z0-9]{1,30}")){
$("#loginGroup").removeClass("has-success")
$("#loginGroup").addClass("has-error")
$("#loginErrorBadChars").removeClass("hidden")
result = false;
} else
{
$("#loginGroup").removeClass("has-error")
$("#loginGroup").addClass("has-success")
}

if (password.length < 1){
$("#passwordGroup").removeClass("has-success")
$("#passwordGroup").addClass("has-error")
$("#passwordErrorEmpty").removeClass("hidden")
result = false;
} else {
$("#passwordGroup").removeClass("has-error")
$("#passwordGroup").addClass("has-success")
}

if (password.length > 0)
if (passwordConfirm != password){
$("#passwordConfirmGroup").removeClass("has-success")
$("#passwordConfirmGroup").addClass("has-error")
$("#confirmPasswordError").removeClass("hidden")
result = false;
} else {
$("#passwordConfirmGroup").removeClass("has-error")
$("#passwordConfirmGroup").addClass("has-success")
}

if (name.length < 1){
$("#nameGroup").removeClass("has-success")
$("#nameGroup").addClass("has-error")
$("#nameErrorEmpty").removeClass("hidden")
result = false;
} else {
$("#nameGroup").removeClass("has-error")
$("#nameGroup").addClass("has-success")
}

if (email.length < 1){
$("#emailGroup").removeClass("has-success")
$("#emailGroup").addClass("has-error")
$("#emailErrorEmpty").removeClass("hidden")
result = false;
} else
if (!email.match("[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}")){
$("#emailGroup").removeClass("has-success")
$("#emailGroup").addClass("has-error")
$("#emailErrorWrong").removeClass("hidden")
result = false;
} else {
$("#emailGroup").removeClass("has-error")
$("#emailGroup").addClass("has-success")
}

return result;
}

function hideErrors(){
$("#serverErrors").hide();
}