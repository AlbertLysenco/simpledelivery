/**
 * Created by albertlysenco on 30.03.14.
 */
$(function () {
    $("#startLocation").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/getwarehouse",
                dataType: "json",

                data: {
                    maxRows: 10,
                    startsWith: request.term
                },
                success: function (data) {
                    response($.map(data.cities, function (item) {

                        return {
                            label: item.city + ", " + item.adress,
                            value: item.city
                        }
                    }));
                }
            });
        },
        minLength: 1,
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });
    $("#destLocation").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/getwarehouse",
                dataType: "json",

                data: {
                    maxRows: 10,
                    startsWith: request.term
                },
                success: function (data) {
                    response($.map(data.cities, function (item) {

                        return {
                            label: item.city + ", " + item.adress,
                            value: item.city
                        }
                    }));
                }
            });
        },
        minLength: 1,
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });
});
