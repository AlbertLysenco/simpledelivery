CREATE DATABASE  IF NOT EXISTS `sdeli` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `sdeli`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: sdeli
-- ------------------------------------------------------
-- Server version	5.5.8

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `validated` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin','$2a$10$Gu/SVEieiALlaaERKGFvduv02Zs8dEoWbbK3CL/p5Krd7gxZBjxx2','ADMIN','admin@sdeli.com','0',NULL),(2,'User One','user1','user1','USER','someemail@mail.com','0',NULL),(3,'user2','user2','user2','USER','sdffsdfsdf','0',NULL),(4,'newuser','newuser','newuser','USER','newuser','0',NULL),(5,'rrr','RRRluxoft','public','USER','rrr.lands@gmail.com','0',NULL),(6,'Картофель','Potato','potato','USER','potato@potatoland.pot','0',NULL),(7,'Картошечка','Potato2','potato2','USER','potato_v2@potatos.lol','0',NULL),(8,'Картофельный Рай','potato3','potato3','USER','potatoes_heaven@potato.com','0',NULL),(9,'test','testPasswordEncoding','asdf','USER','test@test.com','0',NULL),(10,'test1234','testPW','test','USER','test@test.test','0',NULL),(11,'LastPasswordTest','LastPasswordTest','LastPasswordTest','USER','LastPasswordTest@test.com','0',NULL),(12,'LastPasswordTest2','LastPasswordTest2','$2a$10$L1hkSCAK9HLAKWixOpjGougnFvEuFJBn6XVXDxQMCiUVwoRqbRPxm','USER','LastPasswordTest2@test.test','0',NULL),(14,'testSuccessPage','testSuccessPage','$2a$10$64tnAIp4vr96Aihtob5v6eZSqopt9vIJCl4cYaDolwoxNNluaPtu6','USER','test@test.set','0',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-26 21:56:05
