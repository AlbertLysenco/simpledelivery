DROP SCHEMA IF EXISTS sdeli;

CREATE SCHEMA sdeli DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;

USE sdeli;


CREATE TABLE users (
user_id INT PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(255),
login VARCHAR(255) NOT	NULL UNIQUE,
password VARCHAR(255),
role VARCHAR(255),
email VARCHAR(255),
validated VARCHAR(255)
);

CREATE TABLE orders (
order_id INT AUTO_INCREMENT,
sending_warehouse varchar(255) not null,
receiving_warehouse varchar(255) not null,
initial_receive_method varchar(255) not null,
initial_receive_info varchar(255) not null,
weight decimal not null,
heigh decimal not null,
length decimal not null,
width decimal not null,
order_сomment varchar(255),
location varchar(255),
order_status varchar(255) not null,
user_id int not null,
PRIMARY KEY (order_id),
FOREIGN KEY (user_id) REFERENCES users(user_id)
);

INSERT INTO `sdeli`.`users` (`name`,`login`,`password`,`role`, `validated`) VALUES ('admin','admin','admin','ROLE_ADMIN','false');